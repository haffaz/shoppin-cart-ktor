<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Shopping Mania</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>
<body>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Product</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        <#list items as item>
        <tr>
            <td data-th="Product">
                <div class="row">
                    <div class="col-sm-10">
                        <h4 class="nomargin">${item.name}</h4>
                        <p>${item.description}</p>
                    </div>
                </div>
            </td>
            <td data-th="Price">${item.price}</td>
            <td data-th="Quantity">
                <input type="number" class="form-control text-center" value="1">
            </td>
            <td data-th="Subtotal" class="text-center"></td>
            <td class="actions" data-th="">
                <button class="btn btn-info btn-sm"><i class="fa fa-plus"></i></button>
                <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
            </td>
        </tr>
        </#list>
        </tbody>

        <tfoot>
        <tr class="visible-xs">
            <td class="text-center"><strong>Total 1.99</strong></td>
        </tr>
        <tr>
            <td><a href="#" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
            <td colspan="2" class="hidden-xs"></td>
            <td class="hidden-xs text-center"><strong id="total">Total $1.99</strong></td>
            <td><a href="#" class="btn btn-success btn-block" id="checkout">Checkout <i class="fa fa-angle-right"></i></a></td>
        </tr>
        </tfoot>
    </table>
</div>
<#--<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
<#--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>-->

<#--<script>-->

    <#--function addToCart(id) {-->
        <#--alert(id);-->
        <#--$.ajax({-->
            <#--type: 'POST',-->
            <#--url: window.location + "/add",-->
            <#--contentType : 'application/json; charset=utf-8',-->
            <#--dataType: "json",-->
            <#--data:JSON.stringify(id),-->
            <#--success: function (result) {-->
                <#--// alert(result);-->
                <#--console.log("Success:" + result);-->
            <#--},-->
            <#--error: function (e) {-->
                <#--// alert("Error");-->
                <#--console.log("Error:"+ e);-->
            <#--}-->
        <#--})-->
    <#--}-->

    <#--$(document).ready(function () {-->
        <#--ajaxGet();-->

        <#--//DO GET-->
        <#--function ajaxGet() {-->
            <#--$.ajax({-->
                <#--type: "GET",-->
                <#--url: window.location + "/items",-->
                <#--success: function (result) {-->
                    <#--$.each(result, function (i, item) {-->

                        <#--var $tr = $('<tr>').append(-->
                            <#--$('<td data-th="Product">').append(-->
                                <#--$('<div class="row">').append(-->
                                    <#--$('<div class="col-sm-10">').append(-->
                                        <#--$('<h4 class="nomargin">').text(item.name),-->
                                        <#--$('<p>').text(item.description)-->
                                    <#--)-->
                                <#--)-->
                            <#--),-->
                            <#--$('<td data-th="Price">').text("$"+item.price),-->

                            <#--$('<td data-th="Quantity">').append(-->
                                <#--$('<input type="number" class="form-control text-center", value="1" id="quantity"' + i + '>')-->
                            <#--),-->
                            <#--$('<td data-th="Subtotal" class="text-center">').append(-->
                                <#--$('<td class="actions" data-th="">').append(-->
                                    <#--$('<button class="btn btn-info btn-sm"  onclick="addToCart(' + item.id + ')">').append(-->
                                        <#--$('<i class="fa fa-refresh">')-->
                                    <#--),-->
                                    <#--$('<button class="btn btn-danger btn-sm">').append(-->
                                        <#--$('<i class="fa fa-trash-o">')-->
                                    <#--)-->
                                <#--)-->
                            <#--)-->
                        <#--).appendTo('#items');-->

                    <#--});-->
                    <#--console.log("Success: ", result)-->
                <#--},-->
                <#--error: function (e) {-->
                    <#--$("#getItemListDiv").html("<strong>Error</strong>");-->
                    <#--console.log("ERROR: ", e);-->
                <#--}-->
            <#--})-->
        <#--}-->



    <#--})-->
<#--</script>-->
</body>
</html>