package com.example.dao

import com.example.model.Item
import com.example.model.Order

class ShoppingDao {

    private val cart = ArrayList<Order>()
    private val itemList = ArrayList<Item>()

    fun add(item: Item, quantity: Int) {
        val order = Order(item, quantity)
        cart.add(order)
    }

    fun pay(): Double {
        var total = 0.0
        for(order in cart) {
            var price = order.item.price * order.quantity
            total += price
        }

        return total
    }

    fun getAllItems(): ArrayList<Item> {
        itemList.clear()
        //dummy data block: start
        itemList.add(Item(1, "Item1", 12.00, "This is the Dummy Item No 1"))
        itemList.add(Item(2, "Item2", 22.00, "This is the Dummy Item No 2"))
        itemList.add(Item(3, "Item3", 32.00, "This is the Dummy Item No 3"))
        itemList.add(Item(4, "Item3", 42.00, "This is the Dummy Item No 4"))
        itemList.add(Item(5, "Item5", 52.00, "This is the Dummy Item No 5"))
        //dummy data block: end
        return itemList
    }

    fun findItem(id: Int): Item {
        for (item in itemList) {
            if (id == item.id) {
                return item
            }
        }
        throw Exception("Id Not Found")
    }

    fun clear() {
        cart.clear()
    }
}