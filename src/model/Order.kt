package com.example.model

data class Order(val item: Item, var quantity: Int = 1) {
    fun getSubTotal(): Double {
        return item.price*quantity
    }
}
