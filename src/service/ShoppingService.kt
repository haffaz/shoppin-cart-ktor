package com.example.service

import com.example.dao.ShoppingDao
import com.example.model.Item

class ShoppingService {

//    lateinit var shoppingDao: ShoppingDao

    private var shoppingDao : ShoppingDao = ShoppingDao()

    fun add(item: Item, quantity: Int = 1) {
        shoppingDao.add(item, quantity)
    }

    fun pay(): Double {
        return shoppingDao.pay()
    }

    fun getItemList(): ArrayList<Item> {
        return shoppingDao.getAllItems()
    }

    fun findItem(id: Int): Item {
        return shoppingDao.findItem(id)
    }

    fun clear() {
        shoppingDao.clear()
    }
}